package com.sg.dvdlibrary.ui;

import com.sg.dvdlibrary.dto.Dvd;

import java.util.List;

public class DvdLibraryView {
    private UserIO io;

    public DvdLibraryView(UserIO io) {
        this.io = io;
    }

    public void printMenu() {
        io.print("=== Main Menu ===");
        io.print("1. Add a DVD to the library");
        io.print("2. Find a DVD by title");
        io.print("3. Display full library");
        io.print("4. Edit information for an existing DVD");
        io.print("5. Remove a DVD from the library");
        io.print("0. Exit");
    }

    public int getMenuSelection() {
        return io.readInt("Please select from the menu options:", 0, 5);
    }

    public Dvd getNewDvdInfo() {
        io.print("=== Add new DVD ===");
        String title = io.readString("Title:");
        String releaseDate = io.readString("Release date (YYYY.MM.DD format):");
        String mpaaRating = io.readString("MPAA Rating:");
        String directorName = io.readString("Director's name:");
        String studio = io.readString("Studio:");
        String notes = io.readString("Additional Notes:");
        Dvd dvd = new Dvd(title);
        dvd.setReleaseDate(releaseDate.length() > 0 ? releaseDate : " ");
        dvd.setMpaaRating(mpaaRating.length() > 0 ? mpaaRating : " ");
        dvd.setDirectorName(directorName.length() > 0 ? directorName : " ");
        dvd.setStudio(studio.length() > 0 ? studio : " ");
        dvd.setNotes(notes.length() > 0 ? notes : " ");
        return dvd;
    }

    public boolean shouldAddAnother() {
        io.print("DVD successfully added.");
        return io.readYesOrNo("Add another?");
    }

    public String askUserForTitle() {
        return io.readString("Enter title:");
    }

    public void printDvdInfo(Dvd dvd) {
        io.print("Title: " + dvd.getTitle() + "\n"
                + "Release Date: " + dvd.getReleaseDate() + "\n"
                + "MPAA Rating: " + dvd.getMpaaRating() + "\n"
                + "Director: " + dvd.getDirectorName() + "\n"
                + "Studio: " + dvd.getStudio() + "\n"
                + "Notes: " + dvd.getNotes() + "\n");
    }

    public void listAllDvds(List<Dvd> dvds) {
        io.print("=== Display Full Library ===");
        for (Dvd dvd : dvds) {
            printDvdInfo(dvd);
        }
        io.print("Number of DVDs in library: " + dvds.size());
    }

    public Dvd getUpdatedDvdInfo(Dvd dvd) {
        String title = io.readString("Update title or hit return to skip:");
        if (title.length() > 0) {
            dvd.setTitle(title);
        }
        String releaseDate = io.readString("Update release date (YYYY.MM.DD) or hit return to skip:");
        if (releaseDate.length() > 0) {
            dvd.setReleaseDate(releaseDate);
        }
        String mpaaRating = io.readString("Update MPAA rating or hit return to skip:");
        if (mpaaRating.length() > 0) {
            dvd.setMpaaRating(mpaaRating);
        }
        String directorName = io.readString("Update director's name or hit return to skip:");
        if (directorName.length() > 0) {
            dvd.setDirectorName(directorName);
        }
        String studio = io.readString("Update studio or hit return to skip:");
        if (studio.length() > 0) {
            dvd.setStudio(studio);
        }
        String notes = io.readString("Update notes or hit return to skip:");
        if (notes.length() > 0) {
            dvd.setNotes(notes);
        }
        return dvd;
    }

    public void printFindDvdBanner() {
        io.print("=== Search by DVD Title ===");
    }

    public void printDvdNotFound() {
        io.print("No record found.");
    }

    public void printEditDvdBanner() {
        io.print("=== Edit DVD Information ===");
    }

    public boolean shouldEdit() {
        return io.readYesOrNo("Edit this DVD?");
    }

    public void printEditSuccessfulMessage() {
        io.print("DVD successfully updated.");
    }

    public void printEditCancelledMessage() {
        io.print("Update cancelled.");
    }

    public boolean shouldEditAnother() {
        return io.readYesOrNo("Edit another?");
    }

    public void printRemoveDvdBanner() {
        io.print("=== Remove DVD ===");
    }

    public boolean shouldRemove() {
        return io.readYesOrNo("Remove this DVD?");
    }

    public void printRemoveSuccessfulMessage() {
        io.print("DVD successfully removed.");
    }

    public void printRemoveCancelledMessage() {
        io.print("DVD removal cancelled.");
    }

    public boolean shouldRemoveAnother() {
        return io.readYesOrNo("Remove another?");
    }

    public void userEnterToContinue() {
        io.readString("Please hit enter to continue.");
    }

    public void printExitMessage() {
        io.print("Goodbye!");
    }

    public void printErrorMessage(String errorMessage) {
        io.print("=== ERROR ===");
        io.print(errorMessage);
    }
}