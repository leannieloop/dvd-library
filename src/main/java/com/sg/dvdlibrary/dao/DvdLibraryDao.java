package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.Dvd;

import java.util.List;

public interface DvdLibraryDao {
    void addDvds(List<Dvd> dvdsToAdd) throws DvdLibraryPersistenceException;
    Dvd getDvd(String title) throws DvdLibraryPersistenceException;
    int getIndexOfDvd(Dvd dvd) throws DvdLibraryPersistenceException;
    List<Dvd> getAllDvds() throws DvdLibraryPersistenceException;
    void editDvd(int index, Dvd editedDvd) throws DvdLibraryPersistenceException;
    void removeDvd(int index) throws DvdLibraryPersistenceException;
}
